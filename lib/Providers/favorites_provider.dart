import 'package:flutter/material.dart';
import 'package:zema_multimedia_flutter_chalange/Models/favorite_model.dart';
import 'package:zema_multimedia_flutter_chalange/services/favorite_services.dart';

class FavouriteProvider with ChangeNotifier {
  final favouriteservice = FavouriteService();
  List<FavouriteResult> favourites = [];
  List<FavouriteResult> get favouritelist => favourites;
  bool isLoading = false;
  Future<void> getAllFavourites() async {
    isLoading = true;
    notifyListeners();
    final response = await favouriteservice.getAllFavorites();
    favourites = response;
    isLoading = false;
    notifyListeners();
  }
}

import 'package:flutter/material.dart';
import 'package:zema_multimedia_flutter_chalange/Models/music_model.dart';
import 'package:zema_multimedia_flutter_chalange/services/music_service.dart';

class MusicProvider with ChangeNotifier {
  final albumservice = MusicService();
  List<MusicResult> albums = [];
  List<MusicResult> get albumlist => albums;
  bool isLoading = false;
  Future<void> getAllalbums() async {
    isLoading = true;
    notifyListeners();
    final response = await albumservice.getAllMusics();
    albums = response;
    isLoading = false;
    notifyListeners();
  }
}

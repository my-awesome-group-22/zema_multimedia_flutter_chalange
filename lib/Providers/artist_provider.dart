import 'package:flutter/material.dart';
import 'package:zema_multimedia_flutter_chalange/Models/artists_model.dart';
import 'package:zema_multimedia_flutter_chalange/services/artist_services.dart';

class ArtistProvider with ChangeNotifier {
  final artistService = ArtistService();
  List<ArtistResult> artists = [];
  List<ArtistResult> get artistlist => artists;
  bool isLoading = false;
  Future<void> getAllArtists() async {
    isLoading = true;
    notifyListeners();
    final response = await artistService.getAllArtists();
    artists = response;
    isLoading = false;
    notifyListeners();
  }
}

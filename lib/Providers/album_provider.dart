import 'package:flutter/material.dart';
import 'package:zema_multimedia_flutter_chalange/Models/albums_model.dart';
import 'package:zema_multimedia_flutter_chalange/services/album_services.dart';

class AlbumProvider with ChangeNotifier {
  final albumservice = AlbumService();
  List<AlbumResult> albums = [];
  Albums? lazyalbums;
  List<AlbumResult> get albumlist => albums;
  bool isLoading = false;
  Future<void> getAllalbums(int page) async {
    isLoading = true;
    notifyListeners();
    final response = await albumservice.getAllAlbums(page);
    final lazyalbumss = await albumservice.lazyalbum(page);
    albums = response;
    lazyalbums = lazyalbumss;
    isLoading = false;
    notifyListeners();
  }
}

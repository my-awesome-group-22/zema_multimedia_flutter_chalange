import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/album_provider.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/artist_provider.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/favorites_provider.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/music_provider.dart';
import 'package:zema_multimedia_flutter_chalange/config_files/custom_colors.dart';
import 'package:zema_multimedia_flutter_chalange/config_files/custom_sizes.dart';
import 'package:zema_multimedia_flutter_chalange/pages/albums_page.dart';
import 'package:zema_multimedia_flutter_chalange/pages/explore_page.dart';
import 'package:zema_multimedia_flutter_chalange/pages/favorites_page.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<AlbumProvider>(context, listen: false).getAllalbums(1);
      Provider.of<MusicProvider>(context, listen: false).getAllalbums();
      Provider.of<ArtistProvider>(context, listen: false).getAllArtists();
      Provider.of<FavouriteProvider>(context, listen: false).getAllFavourites();
    });
  }

  var radius = const Radius.circular(35);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: DefaultTabController(
            length: 3,
            child: Column(
              children: [
                Container(
                  decoration: const BoxDecoration(
                    color: CustomColors.redcustome,
                  ),
                  child: Center(
                    child: TabBar(
                      indicatorPadding:
                          EdgeInsets.symmetric(vertical: 2.w, horizontal: 5.w),
                      indicator: ShapeDecoration(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(radius)),
                          color: Colors.white),
                      isScrollable: false,
                      padding: EdgeInsets.zero,
                      unselectedLabelStyle: Theme.of(context)
                          .textTheme
                          .bodyMedium
                          ?.copyWith(color: Colors.transparent),
                      labelStyle: Theme.of(context)
                          .textTheme
                          .bodyMedium
                          ?.copyWith(
                              color: CustomColors.customeblack,
                              fontWeight: FontWeight.w600),
                      indicatorSize: TabBarIndicatorSize.tab,
                      labelColor: CustomColors.customeblack,
                      unselectedLabelColor: CustomColors.white,
                      indicatorColor: CustomColors.white,
                      indicatorWeight: 2,
                      labelPadding: EdgeInsets.zero,
                      tabs: const [
                        Tab(text: "Explore"),
                        Tab(text: "Albums "),
                        Tab(text: "Favorites"),
                      ],
                    ),
                  ),
                ),
                const Expanded(
                  child: TabBarView(
                    children: [
                      ExplorePage(),
                      AlbumsPage(),
                      Favoritespage(),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}

import 'dart:ui';

class CustomColors {
  ///primary colors
  static const mainbackground = Color(0xffE6DBDD);
  static const redcustome = Color(0xffFE3562);
  static const customeblack = Color(0xff252525);
  static const white = Color(0xffffffff);
}

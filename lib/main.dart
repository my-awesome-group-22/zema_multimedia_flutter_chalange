import 'package:flutter/material.dart';
import 'package:just_audio_background/just_audio_background.dart';
import 'package:provider/provider.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/album_provider.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/artist_provider.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/favorites_provider.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/music_provider.dart';
import 'package:zema_multimedia_flutter_chalange/Screens/home_Screen.dart';
import 'package:sizer/sizer.dart';

Future<void> main() async {
  await JustAudioBackground.init(
    androidNotificationChannelId: 'com.ryanheise.bg_demo.channel.audio',
    androidNotificationChannelName: 'Audio playback',
    androidNotificationOngoing: true,
  );
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<AlbumProvider>(
          create: (context) => AlbumProvider(),
        ),
        ChangeNotifierProvider<MusicProvider>(
          create: (context) => MusicProvider(),
        ),
        ChangeNotifierProvider<FavouriteProvider>(
          create: (context) => FavouriteProvider(),
        ),
        ChangeNotifierProvider<ArtistProvider>(
          create: (context) => ArtistProvider(),
        ),
      ],
      child: Sizer(builder: (BuildContext context, Orientation orientation,
          DeviceType deviceType) {
        return const MyApp();
      }),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Zema Multimedia Challange',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomeScreen(),
    );
  }
}

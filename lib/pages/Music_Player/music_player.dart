// ignore_for_file: non_constant_identifier_names

import 'package:audio_session/audio_session.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:just_audio/just_audio.dart';
import 'package:just_audio_background/just_audio_background.dart';

import 'package:rxdart/rxdart.dart';
import 'package:sizer/sizer.dart';
import 'package:zema_multimedia_flutter_chalange/config_files/custom_colors.dart';
import 'package:zema_multimedia_flutter_chalange/config_files/custom_sizes.dart';
import 'package:zema_multimedia_flutter_chalange/pages/Music_Player/common.dart';

class MusicPlayer extends StatefulWidget {
  const MusicPlayer(
      {Key? key,
      required this.fileuri,
      required this.imageurl,
      required this.artis_name,
      required this.track_name,
      required this.album})
      : super(key: key);
  final String fileuri, imageurl, artis_name, track_name, album;
  @override
  MusicPlayerState createState() => MusicPlayerState();
}

class MusicPlayerState extends State<MusicPlayer> with WidgetsBindingObserver {
  final _player = AudioPlayer();

  @override
  void initState() {
    super.initState();
    // ambiguate(WidgetsBinding.instance)!.addObserver(this);
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.black,
    ));
    _init();
  }

  Future<void> _init() async {
    final session = await AudioSession.instance;
    await session.configure(const AudioSessionConfiguration.speech());

    _player.playbackEventStream.listen((event) {},
        onError: (Object e, StackTrace stackTrace) {
      print('A stream error occurred: $e');
    });

    try {
      await _player.setAudioSource(AudioSource.uri(
          tag: MediaItem(
            id: '1',
            album: widget.album,
            title: widget.track_name,
            artist: widget.artis_name,
            artUri: Uri.parse(widget.imageurl),
          ),
          Uri.parse(widget.fileuri)));
    } catch (e) {
      print("Error loading audio source: $e");
    }
  }

  @override
  void dispose() {
    _player.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      _player.stop();
    }
  }

  Stream<PositionData> get _positionDataStream =>
      Rx.combineLatest3<Duration, Duration, Duration?, PositionData>(
          _player.positionStream,
          _player.bufferedPositionStream,
          _player.durationStream,
          (position, bufferedPosition, duration) => PositionData(
              position, bufferedPosition, duration ?? Duration.zero));

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: CustomColors.mainbackground,
        appBar: AppBar(
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(Icons.chevron_left)),
          backgroundColor: CustomColors.redcustome,
          title: Text(
            widget.track_name,
            style: TextStyle(
                color: CustomColors.white,
                fontWeight: FontWeight.w700,
                fontSize: 12.sp),
          ),
          centerTitle: true,
          elevation: 0,
        ),
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 6.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 2.w),
                  width: 70.w,
                  height: 50.h,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ClipRRect(
                        borderRadius:
                            BorderRadius.circular(CustomSizes.radius_6),
                        child: Material(
                          elevation: 4,
                          shadowColor: Colors.black.withOpacity(0.2),
                          borderRadius: BorderRadius.circular(
                            CustomSizes.radius_6,
                          ),
                          child: Image.network(
                            widget.imageurl,
                            fit: BoxFit.cover,
                            height: 40.h,
                            width: 80.w,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 5.w,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            widget.track_name,
                            style: TextStyle(
                                color: CustomColors.customeblack,
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w600),
                          ),
                          SizedBox(
                            height: 1.w,
                          ),
                          Text(
                            widget.artis_name,
                            style: TextStyle(
                                color: CustomColors.customeblack,
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                StreamBuilder<PositionData>(
                  stream: _positionDataStream,
                  builder: (context, snapshot) {
                    final positionData = snapshot.data;
                    return SeekBar(
                      duration: positionData?.duration ?? Duration.zero,
                      position: positionData?.position ?? Duration.zero,
                      bufferedPosition:
                          positionData?.bufferedPosition ?? Duration.zero,
                      onChangeEnd: _player.seek,
                    );
                  },
                ),
                Text(
                  "playing ${widget.track_name} from album ${widget.album}",
                  style: TextStyle(
                      color: CustomColors.customeblack,
                      fontSize: 12.sp,
                      fontWeight: FontWeight.w500),
                ),
                ControlButtons(_player),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ControlButtons extends StatelessWidget {
  final AudioPlayer player;

  const ControlButtons(this.player, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        CircleAvatar(
          backgroundColor: CustomColors.redcustome,
          child: StreamBuilder<double>(
            stream: player.speedStream,
            builder: (context, snapshot) => IconButton(
              icon: const Icon(
                Icons.skip_previous,
                color: CustomColors.white,
              ),
              onPressed: () {},
            ),
          ),
        ),
        StreamBuilder<PlayerState>(
          stream: player.playerStateStream,
          builder: (context, snapshot) {
            final playerState = snapshot.data;
            final processingState = playerState?.processingState;
            final playing = playerState?.playing;
            if (processingState == ProcessingState.loading ||
                processingState == ProcessingState.buffering) {
              return Container(
                margin: const EdgeInsets.all(8.0),
                width: 10.w,
                height: 10.w,
                child: const CircularProgressIndicator(),
              );
            } else if (playing != true) {
              return IconButton(
                icon: const Icon(Icons.play_arrow),
                iconSize: 64.0,
                onPressed: player.play,
              );
            } else if (processingState != ProcessingState.completed) {
              return IconButton(
                icon: const Icon(Icons.pause),
                iconSize: 64.0,
                onPressed: player.pause,
              );
            } else {
              return IconButton(
                icon: const Icon(Icons.replay),
                iconSize: 64.0,
                onPressed: () => player.seek(Duration.zero),
              );
            }
          },
        ),
        CircleAvatar(
          backgroundColor: CustomColors.redcustome,
          child: StreamBuilder<double>(
            stream: player.speedStream,
            builder: (context, snapshot) => IconButton(
              icon: const Icon(
                Icons.skip_next,
                color: CustomColors.white,
              ),
              onPressed: () {},
            ),
          ),
        ),
      ],
    );
  }
}

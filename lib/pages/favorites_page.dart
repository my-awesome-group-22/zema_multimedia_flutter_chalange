import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/album_provider.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/favorites_provider.dart';
import 'package:zema_multimedia_flutter_chalange/config_files/custom_colors.dart';
import 'package:zema_multimedia_flutter_chalange/config_files/custom_sizes.dart';
import 'package:zema_multimedia_flutter_chalange/pages/Music_Player/music_player.dart';

class Favoritespage extends StatefulWidget {
  const Favoritespage({super.key});

  @override
  State<Favoritespage> createState() => _FavoritespageState();
}

class _FavoritespageState extends State<Favoritespage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.mainbackground,
      body: Consumer<FavouriteProvider>(builder: (context, value, child) {
        final favouritelist = value.favourites;
        return Container(
          margin: EdgeInsets.symmetric(vertical: 4.w),
          padding: EdgeInsets.symmetric(horizontal: 3.w),
          child: SingleChildScrollView(
            physics: const NeverScrollableScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Favorite Songs",
                  style: TextStyle(
                      color: CustomColors.customeblack,
                      fontWeight: FontWeight.w700,
                      fontSize: 12.sp),
                ),
                SizedBox(
                  height: 2.w,
                ),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: favouritelist.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      horizontalTitleGap: 4.w,
                      leading: CircleAvatar(
                        backgroundColor: Colors.red,
                        radius: 10.w,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(300),
                          child: Material(
                            elevation: 4,
                            shadowColor: Colors.black.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(
                              CustomSizes.radius_6,
                            ),
                            child: Image.network(
                              favouritelist.isNotEmpty
                                  ? "https://zemastroragev100.blob.core.windows.net/zemacontainer/${favouritelist[index].trackCoverImage}"
                                  : "https://gamdsolutions.com/public/playground_assets/zema-logo.jpg",
                              fit: BoxFit.cover,
                              // height: 20.h,
                              // width: 30.h,
                            ),
                          ),
                        ),
                      ),
                      trailing: Icon(
                        Icons.favorite,
                        size: CustomSizes.icon_size_6,
                        color: CustomColors.white,
                      ),
                      minVerticalPadding: 0,
                      contentPadding: EdgeInsets.symmetric(horizontal: 1.w),
                      title: Text(
                        favouritelist.isNotEmpty
                            ? favouritelist[index].trackName
                            : "Track Name",
                        style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                            color: CustomColors.customeblack,
                            fontSize: CustomSizes.font_12,
                            fontWeight: FontWeight.w700),
                      ),
                      subtitle: Text(
                        favouritelist.isNotEmpty
                            ? favouritelist[index].artistName
                            : "Artist Name",
                        style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                              color: CustomColors.customeblack,
                              fontSize: CustomSizes.font_10,
                            ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => MusicPlayer(
                              fileuri:
                                  "https://zemastroragev100.blob.core.windows.net/zemacontainer/${favouritelist[index].trackAudioFile}",
                              imageurl:
                                  "https://zemastroragev100.blob.core.windows.net/zemacontainer/${favouritelist[index].trackCoverImage}",
                              album: favouritelist[index].albumName,
                              track_name: favouritelist[index].trackName,
                              artis_name: favouritelist[index].artistName,
                            ),
                          ),
                        );

                        // controller.getprofileimage(ImageSource.camera);
                      },
                    );
                  },
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}

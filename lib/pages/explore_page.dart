import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/album_provider.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/artist_provider.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/music_provider.dart';
import 'package:zema_multimedia_flutter_chalange/config_files/custom_colors.dart';
import 'package:zema_multimedia_flutter_chalange/config_files/custom_sizes.dart';
import 'package:zema_multimedia_flutter_chalange/pages/Music_Player/music_player.dart';
import 'package:zema_multimedia_flutter_chalange/pages/new_artist_page.dart';
import 'package:zema_multimedia_flutter_chalange/pages/new_music_page.dart';

class ExplorePage extends StatefulWidget {
  const ExplorePage({super.key});

  @override
  State<ExplorePage> createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.mainbackground,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "New Albums",
              style: TextStyle(
                  color: CustomColors.customeblack,
                  fontWeight: FontWeight.w700),
            ),
            SizedBox(
              height: 4.w,
            ),
            Consumer<AlbumProvider>(builder: (context, value, child) {
              final allalbumlist = value.albums;
              return value.isLoading
                  ? Column(
                      children: [
                        const Center(child: CircularProgressIndicator()),
                        SizedBox(
                          height: 2.w,
                        ),
                        Text(
                          "Loading Albums",
                          style: TextStyle(
                              color: CustomColors.customeblack,
                              fontWeight: FontWeight.w700,
                              fontSize: 12.sp),
                        ),
                      ],
                    )
                  : SizedBox(
                      width: double.infinity,
                      height: 25.h,
                      child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: 4,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: EdgeInsets.symmetric(horizontal: 2.w),
                            width: 70.w,
                            height: 18.h,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 70.w,
                                  height: 18.h,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.w))),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(
                                        CustomSizes.radius_6),
                                    child: Material(
                                      elevation: 4,
                                      shadowColor:
                                          Colors.black.withOpacity(0.2),
                                      borderRadius: BorderRadius.circular(
                                        CustomSizes.radius_6,
                                      ),
                                      child: Image.network(
                                        allalbumlist.isEmpty
                                            ? "https://gamdsolutions.com/public/playground_assets/zema-logo.jpg"
                                            : allalbumlist[index]
                                                .albumCoverImage,
                                        fit: BoxFit.cover,
                                        height: 12.h,
                                        width: 10.h,
                                      ),
                                    ),
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          allalbumlist.isEmpty
                                              ? "Artist Name"
                                              : allalbumlist[index].albumName,
                                          style: TextStyle(
                                              color: CustomColors.customeblack,
                                              fontSize: 10.sp,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        SizedBox(
                                          height: 1.w,
                                        ),
                                        Text(
                                          allalbumlist.isEmpty
                                              ? "Artist Name"
                                              : allalbumlist[index].artistName,
                                          style: TextStyle(
                                              color: CustomColors.customeblack,
                                              fontSize: 8.sp,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ],
                                    ),
                                    IconButton(
                                        onPressed: () {},
                                        icon: const Icon(Icons.more_vert))
                                  ],
                                )
                              ],
                            ),
                          );
                        },
                      ),
                    );
            }),
            SizedBox(
              height: 2.w,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "New Music",
                  style: TextStyle(
                      color: CustomColors.customeblack,
                      fontWeight: FontWeight.w700,
                      fontSize: 12.sp),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => const MusicPagePage(),
                      ),
                    );
                  },
                  child: Text(
                    "See More",
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        color: CustomColors.customeblack,
                        fontWeight: FontWeight.w700,
                        fontSize: 10.sp),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 4.w,
            ),
            Consumer<MusicProvider>(builder: (context, value, child) {
              final musiclist = value.albums;
              return value.isLoading
                  ? Column(
                      children: [
                        const Center(child: CircularProgressIndicator()),
                        SizedBox(
                          height: 2.w,
                        ),
                        Text(
                          "Loading Albums",
                          style: TextStyle(
                              color: CustomColors.customeblack,
                              fontWeight: FontWeight.w700,
                              fontSize: 12.sp),
                        ),
                      ],
                    )
                  : SizedBox(
                      width: double.infinity,
                      height: 23.h,
                      child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: 3,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => MusicPlayer(
                                    fileuri: musiclist[index].trackAudioFile,
                                    artis_name: musiclist[index].artistName,
                                    album: musiclist[index].albumName,
                                    track_name: musiclist[index].trackName,
                                    imageurl: musiclist[index].trackCoverImage,
                                  ),
                                ),
                              );
                            },
                            child: Container(
                              margin: EdgeInsets.symmetric(horizontal: 2.w),
                              width: 40.w,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: 70.w,
                                    height: 16.h,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.w))),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(
                                          CustomSizes.radius_6),
                                      child: Material(
                                        elevation: 4,
                                        shadowColor:
                                            Colors.black.withOpacity(0.2),
                                        borderRadius: BorderRadius.circular(
                                          CustomSizes.radius_6,
                                        ),
                                        child: Image.network(
                                          musiclist.isEmpty
                                              ? "https://gamdsolutions.com/public/playground_assets/zema-logo.jpg"
                                              : musiclist[index]
                                                  .trackCoverImage,
                                          fit: BoxFit.fill,
                                          height: 10.h,
                                          width: 10.h,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            musiclist.isEmpty
                                                ? "Track Name"
                                                : musiclist[index].trackName,
                                            style: TextStyle(
                                                color:
                                                    CustomColors.customeblack,
                                                fontSize: 10.sp,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          SizedBox(
                                            height: 1.w,
                                          ),
                                          Text(
                                            musiclist.isEmpty
                                                ? "Artist Name"
                                                : musiclist[index].artistName,
                                            style: TextStyle(
                                                color:
                                                    CustomColors.customeblack,
                                                fontSize: 8.sp,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ],
                                      ),
                                      IconButton(
                                          onPressed: () {},
                                          icon: const Icon(
                                              Icons.favorite_outline))
                                    ],
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    );
            }),
            SizedBox(
              height: 2.w,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "New Artists",
                  style: TextStyle(
                      color: CustomColors.customeblack,
                      fontWeight: FontWeight.w700,
                      fontSize: 12.sp),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => const ArtistPage(),
                      ),
                    );
                  },
                  child: Text(
                    "See More",
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        color: CustomColors.customeblack,
                        fontWeight: FontWeight.w700,
                        fontSize: 10.sp),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 2.w,
            ),
            Consumer<ArtistProvider>(builder: (context, value, child) {
              final artistlist = value.artists;
              return value.isLoading
                  ? Column(
                      children: [
                        const Center(child: CircularProgressIndicator()),
                        SizedBox(
                          height: 2.w,
                        ),
                        Text(
                          "Loading Albums",
                          style: TextStyle(
                              color: CustomColors.customeblack,
                              fontWeight: FontWeight.w700,
                              fontSize: 12.sp),
                        ),
                      ],
                    )
                  : Container(
                      margin: EdgeInsets.symmetric(vertical: 2.w),
                      width: double.infinity,
                      height: 20.h,
                      child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: 4,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: EdgeInsets.symmetric(horizontal: 2.w),
                            width: 30.w,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(300),
                                  child: Material(
                                    elevation: 4,
                                    shadowColor: Colors.black.withOpacity(0.2),
                                    borderRadius: BorderRadius.circular(
                                      CustomSizes.radius_6,
                                    ),
                                    child: Image.network(
                                      artistlist.isEmpty
                                          ? "https://gamdsolutions.com/public/playground_assets/zema-logo.jpg"
                                          : artistlist[index]
                                              .artistProfileImage,
                                      fit: BoxFit.cover,
                                      height: 10.h,
                                      width: 10.h,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 2.w,
                                ),
                                Text(
                                  artistlist.isEmpty
                                      ? "Artist Name"
                                      : artistlist[index].artistName,
                                  style: TextStyle(
                                      color: CustomColors.customeblack,
                                      fontSize: 10.sp,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    );
            }),
            SizedBox(
              height: 2.w,
            ),
          ],
        ),
      ),
    );
  }
}

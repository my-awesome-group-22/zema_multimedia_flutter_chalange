import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:zema_multimedia_flutter_chalange/Providers/music_provider.dart';
import 'package:zema_multimedia_flutter_chalange/config_files/custom_colors.dart';
import 'package:zema_multimedia_flutter_chalange/config_files/custom_sizes.dart';
import 'package:zema_multimedia_flutter_chalange/pages/Music_Player/music_player.dart';

class MusicPagePage extends StatefulWidget {
  const MusicPagePage({super.key});

  @override
  State<MusicPagePage> createState() => _MusicPagePageState();
}

class _MusicPagePageState extends State<MusicPagePage> {
  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.mainbackground,
      appBar: AppBar(
        backgroundColor: CustomColors.redcustome,
        title: Text(
          "New Music",
          style: TextStyle(
              color: CustomColors.customeblack,
              fontWeight: FontWeight.w700,
              fontSize: 12.sp),
        ),
        centerTitle: true,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Consumer<MusicProvider>(builder: (context, value, child) {
          final musiclist = value.albumlist;
          if (value.isLoading) {
            return Column(
              children: [
                const Center(child: CircularProgressIndicator()),
                SizedBox(
                  height: 2.w,
                ),
                Text(
                  "Loading Music",
                  style: TextStyle(
                      color: CustomColors.customeblack,
                      fontWeight: FontWeight.w700,
                      fontSize: 12.sp),
                ),
              ],
            );
          } else {
            return Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(vertical: 4.w),
              padding: EdgeInsets.symmetric(horizontal: 5.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 4.w,
                  ),
                  GridView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        childAspectRatio: 0.9,
                        crossAxisSpacing: CustomSizes.mp_v_1,
                        mainAxisSpacing: 4.w,
                        maxCrossAxisExtent: 30.h),
                    itemCount: musiclist.length,
                    shrinkWrap: true,
                    padding: EdgeInsets.symmetric(horizontal: 4.w),
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => MusicPlayer(
                                fileuri: musiclist[index].trackAudioFile,
                                artis_name: musiclist[index].artistName,
                                album: musiclist[index].albumName,
                                track_name: musiclist[index].trackName,
                                imageurl: musiclist[index].trackCoverImage,
                              ),
                            ),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 2.w),
                          // width: 60.w,
                          height: 22.h,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ClipRRect(
                                borderRadius:
                                    BorderRadius.circular(CustomSizes.radius_6),
                                child: Material(
                                  elevation: 4,
                                  shadowColor: Colors.black.withOpacity(0.2),
                                  borderRadius: BorderRadius.circular(
                                    CustomSizes.radius_6,
                                  ),
                                  child: Image.network(
                                    musiclist[index].trackCoverImage,
                                    fit: BoxFit.cover,
                                    height: 16.h,
                                    width: 40.w,
                                  ),
                                ),
                              ),
                              Text(
                                musiclist[index].trackName,
                                style: TextStyle(
                                    color: CustomColors.customeblack,
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w600),
                              ),
                              SizedBox(
                                height: 1.w,
                              ),
                              Text(
                                musiclist[index].artistName,
                                style: TextStyle(
                                    color: CustomColors.customeblack,
                                    fontSize: 8.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            );
          }
        }),
      ),
    );
  }
}

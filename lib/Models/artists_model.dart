// To parse this JSON data, do
//
//     final artists = artistsFromJson(jsonString);

import 'dart:convert';

Artists artistsFromJson(String str) => Artists.fromJson(json.decode(str));

String artistsToJson(Artists data) => json.encode(data.toJson());

class Artists {
  Artists({
    required this.count,
    required this.next,
    this.previous,
    required this.artistresult,
  });

  int count;
  String next;
  dynamic previous;
  List<ArtistResult> artistresult;

  factory Artists.fromJson(Map<String, dynamic> json) => Artists(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        artistresult: List<ArtistResult>.from(
            json["results"].map((x) => ArtistResult.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(artistresult.map((x) => x.toJson())),
      };
}

class ArtistResult {
  ArtistResult({
    required this.id,
    required this.artistName,
    required this.artistDescription,
    required this.artistProfileImage,
  });

  int id;
  String artistName;
  String artistDescription;
  String artistProfileImage;

  factory ArtistResult.fromJson(Map<String, dynamic> json) => ArtistResult(
        id: json["id"],
        artistName: json["artist_name"],
        artistDescription: json["artist_description"],
        artistProfileImage: json["artist_profileImage"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "artist_name": artistName,
        "artist_description": artistDescription,
        "artist_profileImage": artistProfileImage,
      };
}

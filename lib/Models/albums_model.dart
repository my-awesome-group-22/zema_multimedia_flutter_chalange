// To parse this JSON data, do
//
//     final albums = albumsFromJson(jsonString);

import 'dart:convert';

Albums albumsFromJson(String str) => Albums.fromJson(json.decode(str));

String albumsToJson(Albums data) => json.encode(data.toJson());

class Albums {
  Albums({
    required this.count,
    required this.next,
    this.previous,
    required this.albumresults,
  });

  int count;
  String next;
  dynamic previous;
  List<AlbumResult> albumresults;

  factory Albums.fromJson(Map<String, dynamic> json) => Albums(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        albumresults: List<AlbumResult>.from(
            json["results"].map((x) => AlbumResult.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(albumresults.map((x) => x.toJson())),
      };
}

class AlbumResult {
  AlbumResult({
    required this.id,
    required this.albumName,
    required this.albumDescription,
    required this.albumCoverImage,
    required this.artistName,
  });

  int id;
  String albumName;
  String albumDescription;
  String albumCoverImage;
  String artistName;

  factory AlbumResult.fromJson(Map<String, dynamic> json) => AlbumResult(
        id: json["id"],
        albumName: json["album_name"],
        albumDescription: json["album_description"],
        albumCoverImage: json["album_coverImage"],
        artistName: json["artist_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "album_name": albumName,
        "album_description": albumDescription,
        "album_coverImage": albumCoverImage,
        "artist_name": artistName,
      };
}

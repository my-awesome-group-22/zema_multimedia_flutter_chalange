// To parse this JSON data, do
//
//     final favourite = favouriteFromJson(jsonString);

import 'dart:convert';

Favourite favouriteFromJson(String str) => Favourite.fromJson(json.decode(str));

String favouriteToJson(Favourite data) => json.encode(data.toJson());

class Favourite {
  Favourite({
    required this.count,
    this.next,
    this.previous,
    required this.favouriteresult,
  });

  int count;
  dynamic next;
  dynamic previous;
  List<FavouriteResult> favouriteresult;

  factory Favourite.fromJson(Map<String, dynamic> json) => Favourite(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        favouriteresult: List<FavouriteResult>.from(
            json["results"].map((x) => FavouriteResult.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(favouriteresult.map((x) => x.toJson())),
      };
}

class FavouriteResult {
  FavouriteResult({
    required this.id,
    required this.userFui,
    required this.trackName,
    required this.trackDescription,
    required this.trackCoverImage,
    required this.trackAudioFile,
    required this.artistName,
    required this.albumName,
    required this.genreName,
  });

  int id;
  String userFui;
  String trackName;
  String trackDescription;
  String trackCoverImage;
  String trackAudioFile;
  String artistName;
  String albumName;
  String genreName;

  factory FavouriteResult.fromJson(Map<String, dynamic> json) =>
      FavouriteResult(
        id: json["id"],
        userFui: json["user_FUI"],
        trackName: json["track_name"],
        trackDescription: json["track_description"],
        trackCoverImage: json["track_coverImage"],
        trackAudioFile: json["track_audioFile"],
        artistName: json["artist_name"],
        albumName: json["album_name"],
        genreName: json["genre_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_FUI": userFui,
        "track_name": trackName,
        "track_description": trackDescription,
        "track_coverImage": trackCoverImage,
        "track_audioFile": trackAudioFile,
        "artist_name": artistName,
        "album_name": albumName,
        "genre_name": genreName,
      };
}

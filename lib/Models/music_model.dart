// To parse this JSON data, do
//
//     final musics = musicsFromJson(jsonString);

import 'dart:convert';

Musics musicsFromJson(String str) => Musics.fromJson(json.decode(str));

String musicsToJson(Musics data) => json.encode(data.toJson());

class Musics {
  Musics({
    required this.count,
    required this.next,
    this.previous,
    required this.musicresults,
  });

  int count;
  String next;
  dynamic previous;
  List<MusicResult> musicresults;

  factory Musics.fromJson(Map<String, dynamic> json) => Musics(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        musicresults: List<MusicResult>.from(
            json["results"].map((x) => MusicResult.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(musicresults.map((x) => x.toJson())),
      };
}

class MusicResult {
  MusicResult({
    required this.id,
    required this.trackName,
    required this.trackDescription,
    required this.trackCoverImage,
    required this.trackAudioFile,
    required this.artistName,
    required this.albumName,
    required this.genreName,
  });

  int id;
  String trackName;
  String trackDescription;
  String trackCoverImage;
  String trackAudioFile;
  String artistName;
  String albumName;
  String genreName;

  factory MusicResult.fromJson(Map<String, dynamic> json) => MusicResult(
        id: json["id"],
        trackName: json["track_name"],
        trackDescription: json["track_description"],
        trackCoverImage: json["track_coverImage"],
        trackAudioFile: json["track_audioFile"],
        artistName: json["artist_name"],
        albumName: json["album_name"],
        genreName: json["genre_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "track_name": trackName,
        "track_description": trackDescription,
        "track_coverImage": trackCoverImage,
        "track_audioFile": trackAudioFile,
        "artist_name": artistName,
        "album_name": albumName,
        "genre_name": genreName,
      };
}

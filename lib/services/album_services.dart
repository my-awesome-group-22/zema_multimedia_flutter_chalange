import 'package:zema_multimedia_flutter_chalange/Models/albums_model.dart';
import 'package:zema_multimedia_flutter_chalange/Models/albums_model.dart';
import 'package:http/http.dart' as http;

class AlbumService {
  Future<List<AlbumResult>> getAllAlbums(int page) async {
    try {
      var url = Uri.parse(
          "http://exam.calmgrass-743c6f7f.francecentral.azurecontainerapps.io/albums?page=$page");
      var client = http.Client();
      List<AlbumResult> albums = [];
      final response = await client.get(url);
      if (response.statusCode == 200) {
        final Albums allalbums = albumsFromJson(response.body);
        print(allalbums.albumresults);
        for (var album in allalbums.albumresults.toList()) {
          albums.add(album);
        }
        print("categories data$albums");

        return allalbums.albumresults;
      } else {
        throw Exception('failed to load Albums');
      }
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<Albums> lazyalbum(int page) async {
    try {
      var url = Uri.parse(
          "http://exam.calmgrass-743c6f7f.francecentral.azurecontainerapps.io/albums?page=$page");
      var client = http.Client();

      final response = await client.get(url);
      if (response.statusCode == 200) {
        final Albums allalbums = albumsFromJson(response.body);
        print(allalbums.albumresults);

        return allalbums;
      } else {
        throw Exception('failed to load Albums');
      }
    } catch (e) {
      throw Exception(e);
    }
  }
}

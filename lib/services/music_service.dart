import 'package:zema_multimedia_flutter_chalange/Models/music_model.dart';
import 'package:http/http.dart' as http;

class MusicService {
  Future<List<MusicResult>> getAllMusics() async {
    try {
      var url = Uri.parse(
          "http://exam.calmgrass-743c6f7f.francecentral.azurecontainerapps.io/tracks");
      var client = http.Client();
      List<MusicResult> music = [];
      final response = await client.get(url);
      if (response.statusCode == 200) {
        final Musics allmusic = musicsFromJson(response.body);
        print(allmusic.musicresults);
        for (var album in allmusic.musicresults.toList()) {
          music.add(album);
        }
        print("categories data$music");

        return allmusic.musicresults;
      } else {
        throw Exception('failed to load Musics');
      }
    } catch (e) {
      print("print music error $e");
      throw Exception(e);
    }
  }
}

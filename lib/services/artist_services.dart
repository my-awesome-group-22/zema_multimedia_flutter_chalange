import 'package:zema_multimedia_flutter_chalange/Models/artists_model.dart';
import 'package:http/http.dart' as http;

class ArtistService {
  Future<List<ArtistResult>> getAllArtists() async {
    try {
      var url = Uri.parse(
          "http://exam.calmgrass-743c6f7f.francecentral.azurecontainerapps.io/artists");
      var client = http.Client();
      List<ArtistResult> artists = [];
      final response = await client.get(url);
      if (response.statusCode == 200) {
        final Artists allartists = artistsFromJson(response.body);
        print(allartists);
        for (var album in allartists.artistresult.toList()) {
          artists.add(album);
        }
        print("categories data artist$artists");

        return allartists.artistresult;
      } else {
        throw Exception('failed to load Artists');
      }
    } catch (e) {
      print("printing artist error $e");
      throw Exception(e);
    }
  }
}

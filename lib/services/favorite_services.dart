import 'package:http/http.dart' as http;
import 'package:zema_multimedia_flutter_chalange/Models/favorite_model.dart';

class FavouriteService {
  Future<List<FavouriteResult>> getAllFavorites() async {
    try {
      var url = Uri.parse(
          "http://exam.calmgrass-743c6f7f.francecentral.azurecontainerapps.io/favourites");
      var client = http.Client();
      List<FavouriteResult> favorites = [];
      final response = await client.get(url);
      if (response.statusCode == 200) {
        final Favourite allfavorites = favouriteFromJson(response.body);
        print(allfavorites.favouriteresult);
        for (var album in allfavorites.favouriteresult.toList()) {
          favorites.add(album);
        }

        return allfavorites.favouriteresult;
      } else {
        throw Exception('failed to load Albums');
      }
    } catch (e) {
      throw Exception(e);
    }
  }
}
